import { ImfaPage } from './app.po';

describe('imfa App', function() {
  let page: ImfaPage;

  beforeEach(() => {
    page = new ImfaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
